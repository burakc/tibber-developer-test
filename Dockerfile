FROM node:alpine
EXPOSE 5000 9229 5432
WORKDIR /home/app
COPY package.json /home/app/
COPY package-lock.json /home/app/
RUN npm ci
COPY . /home/app
RUN npm run
CMD ./scripts/start.sh