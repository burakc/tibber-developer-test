CREATE TABLE IF NOT EXISTS executions (
  "id" SERIAL PRIMARY KEY,
  "timestamp" timestamptz(6) DEFAULT now(),
  "commands" int4,
  "result" int4,
  "duration" float4
);
