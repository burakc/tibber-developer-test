# Tibber Developer Test API

## How to install
**1. Clone the repo on your server**

    git clone https://burakc@bitbucket.org/burakc/tibber-developer-test.git
**2. Copy env.example to create new custom setting for your environment**

    $ cp env-example .env
**3. If you need to change environment setting please edit .env file. Default settings meets the required configuration. You can set the environment type production or development**  

    NODE_ENV=production
    NODE_PORT=5000
    UTC_TIMEZONE_OFFSET=+2
    PG_HOST=postgres
    PG_DB=postgres
    PG_USER=postgres
    PG_PASSWORD=postgres
    PG_PORT=5432

**4. Build and run docker containers in detached mode**

    docker-compose up --build -d

## Methods
**Enter Path (POST)**  
You can send raw format of JSON request. The structure of JSON example illustrated below.  
**URL:**

    http://localhost:5000/tibber-developer-test/enter-path
**Example Request: Content-Type: application/json**  

    {
        "start": {
            "x": 0,
            "y": 0
        },
        "commands": [
            {
                "direction": "east",
                "steps": 2
            },
            {
                "direction": "north",
                "steps": 1
            }
        ]
    }
**Example Response:**

    {
        "id": 1,
        "timestamp": "2019-11-03 18:18:36.157000 +02:00",
        "commands": 2,
        "result": 4,
        "duration": 0.559626
    }
