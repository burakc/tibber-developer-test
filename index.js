const perf = require('execution-time')()
const pg = require("pg")
const express = require('express')
const bodyParser = require('body-parser')
const moment = require('moment')
const app = express()
const config = {
	host: process.env.PG_HOST,
	database: process.env.PG_DB,
	user: process.env.PG_USER,
	password: process.env.PG_PASSWORD,
	port: process.env.PG_PORT,
}
const pool = new pg.Pool(config)
app.use(bodyParser.urlencoded({
	extended: true
}))
app.use(bodyParser.json())
const offset = 100000
// Create 2D Array
const cleaned_places = new Array(2)
for (i = 0; i <= offset * 2; i++) {
	cleaned_places[i] = []
}

app.post('/tibber-developer-test/enter-path', async (request, response) => {
	perf.start()
	const localMoment = moment()
	const utcMoment = moment.utc()
	const timestamp_db = utcMoment.utcOffset(parseInt(process.env.UTC_TIMEZONE_OFFSET)).format("YYYY-MM-DD HH:mm:ss.SSSSSS")
	const timestamp_json = utcMoment.utcOffset(parseInt(process.env.UTC_TIMEZONE_OFFSET)).format("YYYY-MM-DD HH:mm:ss.SSSSSS Z")
	const path = request.body
	const total_commands = Object.keys(path.commands).length
	var start_x = path.start.x + offset
	var start_y = path.start.y + offset
	var finish_x = 0
	var finish_y = 0
	for (var index = 0; index < path.commands.length; index++) {
		const steps = path.commands[index].steps
		if (index == 0) {
			cleaned_places[start_x][start_y] = true
		}
		if (path.commands[index].direction == 'east') {
			var destination = start_x + steps
			while (start_x < destination) {
				start_x++
				cleaned_places[start_x][start_y] = true
			}
		}
		if (path.commands[index].direction == 'west') {
			var destination = start_x - steps
			while (start_x > destination) {
				start_x--
				cleaned_places[start_x][start_y] = true
			}
		}
		if (path.commands[index].direction == 'north') {
			var destination = start_y + steps
			while (start_y < destination) {
				start_y++
				cleaned_places[start_x][start_y] = true
			}
		}
		if (path.commands[index].direction == 'south') {
			var destination = start_y - steps
			while (start_y > destination) {
				start_y--
				cleaned_places[start_x][start_y] = true
			}
		}
	}
	var results_counter = 0
	cleaned_places.forEach(element => {
		element.forEach(inner_element => {
			results_counter++
		})
	})
	const elapsed = perf.stop()
	const duration = (elapsed.time / 100).toFixed(6)
	pool.connect((err, client, done) => {
		if (err) {
			response.status(500).json({message: err.message})
		}
		client.query(
			'INSERT INTO executions (commands,result,duration,timestamp) VALUES($1, $2, $3, $4) RETURNING *',
			[path.commands.length, results_counter, duration, timestamp_db],
			(err, result) => {
				done()
				if (err) {
					response.status(400).json({message: err.message})
				}
				result.rows[0].timestamp = timestamp_json
				response.status(200).json(result.rows[0])
			})
	})
})

app.listen(process.env.NODE_PORT, () => {
	if (process.env.NODE_ENV == 'development') {
		console.log("Listenning port " + process.env.NODE_PORT)
	}
})
